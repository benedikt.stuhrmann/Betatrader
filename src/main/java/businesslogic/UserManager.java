package businesslogic;

import data.DAOFactory;
import data.DAOFactory.Backend;
import data.UserDAO;

public class UserManager {
	UserDAO userDAO;
	
	public UserManager() {
		super();
		userDAO = DAOFactory.getDAOFactory(Backend.H2).getUserDAO();
	}
}
