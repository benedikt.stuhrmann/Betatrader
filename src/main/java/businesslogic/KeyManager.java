package businesslogic;

import data.DAOFactory;
import data.DAOFactory.Backend;
import data.KeyDAO;

public class KeyManager {
	KeyDAO keyDAO;
	
	public KeyManager() {
		super();
		keyDAO = DAOFactory.getDAOFactory(Backend.H2).getKeyDAO();
	}
}
